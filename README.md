## Faderfox PC4 LCD Preset Editor

Simple web interface to create and save presets for a custom serial LCD display 
used with the Faderfox PC4 midi controller.

![image](https://gitlab.com/kafkaa/faderfox-lcd-editor/-/raw/master/editor.png)

### Overview

Uses a Flask server to spin up a browser interface. 

Create and edit up to 12 presets for the LCD (or whatever might be useful). 
Uses a local sqlite3 database. The LCD interface is found within the midi-tool 
application in this repo.

### Requirements

+ [flask](https://flask.palletsprojects.com)
+ [sqlite3](https://www.sqlite.org/index.html)
+ [pipenv](https://pipenv.pypa.io/en/latest/)
+ [python](https://www.python.org)

### Getting Started
    
    git clone https://gitlab.com/kafkaa/faderfox-lcd-editor.git
    cd faderfox
    pipenv install
    pipenv shell
    python3 run.py
    open web browser and navigate to http://127.0.0.1:5000

### Reference

+ [faderfox PC4](http://faderfox.de/pc4.html)
