import sqlite3

def store(db: str, name: str, controllers: list):
    try:
        with sqlite3.connect(db) as conn:
            curs = conn.cursor()
            curs.execute(f"DROP TABLE IF EXISTS {name}") 
            curs.execute(f"CREATE TABLE {name}(id INTEGER, label TEXT)")
            curs.executemany(f"INSERT INTO {name} VALUES (?, ?)", controllers)

        return None

    except sqlite3.Error as error:
        return error


def fetch(db: str, table: str):
    try:
        connector = sqlite3.connect(db)
        cursor = connector.cursor()
        cursor.execute(f"SELECT * FROM {table}")
        result = cursor.fetchall()
        connector.close()
        return result

    except sqlite3.Error as error:
        print(f'DATABASE FETCH ERROR: {error}')
        return None


def fetch_tables(db: str):
    try:
        connector = sqlite3.connect(db)
        cursor = connector.cursor()
        cursor.execute(f"SELECT name FROM sqlite_master WHERE type='table'")
        result = cursor.fetchall()
        connector.close()
        return result

    except sqlite3.Error as error:
        print(f'DATABASE FETCH ERROR: {error}')
        return None


def table_exists(db: str, name: str) -> bool:
    try:
        connector = sqlite3.connect(db)
        cursor = connector.cursor()
        cursor.execute(f"SELECT count(*) FROM sqlite_master WHERE type='table' AND name='{name}';")
        return cursor.fetchone()[0]

    except sqlite3.Error as error:
        print(f'DATA EXISTS ERROR: {error}')
        return None


def fetch_presets(db) -> list:
    presets = fetch_tables(db)
    presets = [preset[0] for preset in presets]
    length = len(presets)
    if length < 12:
        extends = [f'Preset {n + length + 1}' for n in range(12-length)]
        presets.extend(extends)
    return presets[:12]


def load_preset(db: str, preset: str) -> dict:
    try:
        loaded = dict(fetch(db, preset))
        loaded = {f"cc{k}": v for k, v in loaded.items()}
        loaded['preset'] = preset
        return loaded

    except TypeError:
        return None


def delete_preset(db: str, table: str):
    try:
        with sqlite3.connect(db) as conn:
            curs = conn.cursor()
            curs.execute(f"DROP TABLE IF EXISTS {table}") 

        return None

    except sqlite3.Error as error:
        return error





