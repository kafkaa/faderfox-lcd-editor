import pathlib
from flask import url_for
from flask import request
from flask import redirect
from flask import Blueprint
from flask import render_template
from urllib.parse import urlparse

from faderfox.main.forms import ControllersForm
from faderfox.main.dataops import load_preset
from faderfox.main.dataops import store, fetch 
from faderfox.main.dataops import delete_preset
from faderfox.main.dataops import fetch_presets
 

main = Blueprint('main', __name__)

DB = pathlib.Path('persistence/presets.db')

@main.route('/', methods=['GET', 'POST'])
@main.route('/new', methods=['GET', 'POST'])
def new():
    form = ControllersForm(meta={'csrf': False})
    if request.method == 'POST':
        if form.validate():
            data = dict(request.form)
            preset = data.pop('preset')
            transformed = {(int(k.strip('cc'))): ('unassigned' if not v else v) for k,v in data.items()}
            controllers = sorted(transformed.items(), key=lambda items: items[0])
            err = store(DB, preset, controllers)
            return redirect(url_for('main.new', saved=True, name=preset, error=err))
    return render_template('index.html', presets=fetch_presets(DB), form=form, **request.args)


@main.route('/load/<preset>', methods=['GET'])
def load(preset):
    data = load_preset(DB, preset)
    if data:
        form = ControllersForm(data=data, meta={'csrf': False})
        return render_template('index.html', presets=fetch_presets(DB), form=form, saved=False)
    return redirect(url_for('main.new'))


@main.route('/delete', methods=['GET'])
def delete():
    preset = urlparse(request.referrer).path.split('/')[-1]
    if preset and preset != 'new':
        err = delete_preset(DB, preset)
        return redirect(url_for('main.new', deleted=True, name=preset, error=err))
    return redirect(url_for('main.new'))
