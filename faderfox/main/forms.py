from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired, Length, Optional

from decolab.classes import attributes

cnames = [f'cc{n}' for n in range(1, 25)]
valids = [Optional(), Length(min=2, max=16)]
fields = [StringField(f'CC {n}', validators=valids) for n in range(24)]

@attributes(dict(zip(cnames, fields)))
class ControllersForm(FlaskForm):
    preset = StringField('Preset Name', validators=[DataRequired(), Length(min=2, max=16)])
    submit = SubmitField('Save Preset')

    def get(self, num: int):
        return getattr(self, f"cc{num}")
