from flask import Flask
from flask import url_for
from faderfox.main.routes import main

app = Flask(__name__)
app.register_blueprint(main)

